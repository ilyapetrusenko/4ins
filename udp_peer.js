const dgram = require('dgram');
const server = dgram.createSocket('udp4');
const client = dgram.createSocket('udp4');

const id = process.argv[2];
if (!id) throw('Where is the instanse id?');
const message = new Buffer(id);

const interval = 5000;
const peers = {};

server.on('message', (msg, remote) => {
    if (msg.toString() == id) return;
    peers[remote.address] = Date.now();
    console.log(msg.toString(), 'from', remote.address);
});
server.bind(4000, () => console.log('Peer listening 4000/udp'));

client.bind(() => {
    client.setBroadcast(true);
    client.send(message, 0, message.length, 4000, '255.255.255.255');

    setInterval(() => {
        for (let ip in peers) {
            if (peers[ip] < interval * 5) return delete peers[ip];
            client.send(message, 0, message.length, 4000, ip);
        }
    }, interval);
});
