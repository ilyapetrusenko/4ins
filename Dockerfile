FROM node:8
ADD  udp_peer.js /opt/udp_peer.js

ENTRYPOINT ["node", "/opt/udp_peer.js"]

